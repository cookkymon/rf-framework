*** Settings ***
Resource          Library.txt
Resource          DataValidation.txt

*** Keywords ***
[EW] Get Data All Row For Excel
    [Arguments]    ${Filename}    ${SheetName}    ${TestName}
    Set Library Search Order    ExcelRobot
    ${CountColumn}    ${CountRow}    [ER] Open Excel    ${Filename}    ${SheetName}
    ${EXECUTETEST}    Create List
    FOR    ${I}    IN RANGE    1    ${CountRow}
        ${Return}    [ER] Read Cell Data Column For Excel    ${SheetName}    ${CountColumn}    ${I}
        Append To List    ${EXECUTETEST}    ${Return}
    END
    Comment    ${RESULT}    REPORT SET
    Comment    [EW] Open Excel To Write    ${Filename}    ${SheetName}    ${RESULT}
    FOR    ${I}    IN    @{EXECUTETEST}
        [EW] Write Cell Status For Excel    ${RESULT}    A    No.    @{I}[0]    @{I}[0]
        #    [EW] Write Cell Status For Excel    ${RESULT}    B    STATUS    @{I}[0]    @{I}[1]
        [EW] Write Cell Test Case For Excel    ${RESULT}    C    TESTCASE_NAME    @{I}[0]    @{I}[2]
        LOG    "@{I}[2]"!="${TestName}"
        Run Keyword If    "@{I}[2]"!="${TestName}"    Continue For Loop
        LOG    "@{I}[1]"=="PASS" or "@{I}[1]"=="NOT EXECUTE"
        Run Keyword If    "@{I}[1]"=="PASS" or "@{I}[1]"=="NOT EXECUTE"    Continue For Loop
        ${Date}    Get Current Date
        [EW] Write Cell Message For Excel    ${RESULT}    E    START TIME    @{I}[0]    ${Date}
        ${STATUS}    ${ERROR}    Run Keyword And Ignore Error    @{I}[2]
        Open Excel To Write    ${FileExcelWrite}
        ${Date}    Get Current Date
        [EW] Write Cell Message For Excel    ${RESULT}    F    END TIME    @{I}[0]    ${Date}
        [EW] Write Cell Status For Excel    ${RESULT}    B    STATUS    @{I}[0]    ${STATUS}
        Run Keyword If    "${STATUS}"=="FAIL"    [EW] Write Cell Error For Excel    ${RESULT}    D    RESULT    @{I}[0]    ${ERROR}
        Run Keyword If    "${STATUS}"=="FAIL"    Fail    ${ERROR}
    END

[EW] Open Excel To Write
    [Arguments]    ${Filename}    ${SheetName}    ${RESULTSheet}
    Set Library Search Order    ExcelRobot
    ${Filename}    Join Path    ${Filename}
    ${DIR}    Remove String    ${CURDIR}    FRAMEWORKS
    ${FileExcelWrite}    Set Variable    ${DIR}${/}${Filename}
    Set Suite Variable    ${FileExcelWrite}
    Open Excel To Write    ${FileExcelWrite}
    Create Sheet    ${RESULTSheet}

[EW] Write Cell Error For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${ERROR}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}+1
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${ERROR}
    Save Excel

[EW] Write Cell Status For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${STATUS}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}+1
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${STATUS}
    Save Excel

[EW] Write Cell Test Case For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${TESTCASENAME}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}+1
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${TESTCASENAME}
    Save Excel

[EW] Write Cell Message For Excel
    [Arguments]    ${SheetName}    ${Column}    ${Title}    ${Row}    ${Msg}
    Set Library Search Order    ExcelRobot
    ${Row}    Evaluate    ${Row}+1
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${Msg}
    Save Excel

[EW] Write Cell Message To Excel
    [Arguments]    ${Filename}    ${SheetName}    ${Column}    ${Title}    ${Row}    ${Msg}
    Set Library Search Order    ExcelRobot
    ${Filename}    Join Path    ${Filename}
    ${DIR}    Remove String    ${CURDIR}    FRAMEWORKS
    ${DIR}    Set Variable    ${DIR}${/}${Filename}
    Open Excel To Write    ${DIR}
    Write To Cell By Name    ${SheetName}    ${Column}1    ${Title}
    Write To Cell By Name    ${SheetName}    ${Column}${Row}    ${Msg}
    Save Excel
